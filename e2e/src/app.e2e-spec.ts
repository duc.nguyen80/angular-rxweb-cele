import { AppPage } from './app.po';

describe('coface App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getUserInfoText()).toEqual('User info');
  });
});
