import { expect } from 'chai';
import { defineSupportCode, setDefaultTimeout } from 'cucumber';
import { AppPage } from './app.po';
import { browser, by, element } from 'protractor';

setDefaultTimeout(60 * 1000);

defineSupportCode(({ Given, When, Then, Before }) => {
    let page: AppPage;
    const request = 'Take two strokes off my golf game';

    Before(() => {
        page = new AppPage();
    });

    Given('I am on the home page',
        () => page.navigateTo());

    // When('I fill the request',
    //     () => page.addRequest(request));

    // When('I create A Meeseeks',
    //     () => page.ValidateMeeseeks());

    // Then('The Meeseeks should fulfill my wish',
    //     async () => expect(await page.getMeeseeksResponse()).equals('Caaaan do ' + request + ', yes sireee'));

    Then('The Text ',
        async () => expect(await page.getUserInfoText()).equals('User Info'));
});