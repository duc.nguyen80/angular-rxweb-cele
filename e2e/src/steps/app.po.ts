import { browser, by, element, until } from 'protractor';

export class AppPage {
    navigateTo() {
        return browser.get('/');
    }

    addRequest(request: string) {
        return element(by.id('request'))
            .sendKeys(request);
    }

    ValidateMeeseeks() {
        return element(by.buttonText('Validate Meeseeks'))
            .click();
    }

    getMeeseeksResponse() {
        return element.all(by.id('response')).first().getText();
    }



    getUserInfoText() {
        return element(by.id('userInfo')).getText();
    }
}